from fabric.api import *

env.hosts = ['128.199.139.197']

# Username
env.user = "root"

db_user = "loraserver"
db_pass = "dbpassword"
db_table = "loraserver"

# Install Ubuntu package
def install_package():
	# Update and upgrade apt
	run("apt-get update")
	run("apt-get upgrade")

	# install mosquitto, redis and postgresql
	run("apt-get install mosquitto")
	run("apt-get install redis-server")
	run("apt-get install postgresql")

# Add system user
def add_system_users():
	run("useradd -M -r -s /bin/false/gatewaybridge")
	run("useradd -M -r -s /bin/false/loraserver")
	run("useradd -M -r -s /bin/false/appserver")

# Create Database
def create_database():
	sudo('psql -c "CREATE USER %s WITH NOCREATEDB NOCREATEUSER " \
         "ENCRYPTED PASSWORD E\'%s\'"' % (db_user, db_pass), user='postgres')
	sudo('psql -c "CREATE DATABASE %s WITH OWNER %s"' % (db_table, db_user), user='postgres')

# Download and unpack the pre-compiled binary
def download_and_unpack():

	# LoRa Gateway Bridge Version 2.1.2
	run("wget https://github.com/brocaar/lora-gateway-bridge/releases/download/2.1.2/lora_gateway_bridge_2.1.2_linux_amd64.tar.gz")
	run("tar zxf lora_gateway_bridge_2.1.2_linux_amd64.tar.gz")
	run("mkdir -p /opt/lora-gateway-bridge/bin")
	run("mv lora-gateway-bridge /opt/lora-gateway-bridge/bin")

	# LoRa Network Server Version Version 0.12.5
	run("wget https://github.com/brocaar/loraserver/releases/download/0.12.5/loraserver_0.12.5_linux_amd64.tar.gz")
	run("tar zxf loraserver_0.12.5_linux_amd64.tar.gz")
	run("mkdir -p /opt/loraserver/bin")
	run("mv loraserver /opt/loraserver/bin")

	# LoRa App Server Version 0.1.4
	run("wget https://github.com/brocaar/lora-app-server/releases/download/0.1.4/lora_app_server_0.1.4_linux_amd64.tar.gz")
	run("tar zxf lora_app_server_0.1.4_linux_amd64.tar.gz")
	run("mkdir -p /opt/lora-app-server/bin")
	run("mv lora-app-server /opt/lora-app-server/bin")

# Create a self-signed certificate
def create_certificate():
	run("mkdir -p /opt/lora-app-server/certs")
	run("openssl req -x509 -newkey rsa:4096 -keyout /opt/lora-app-server/certs/http-key.pem -out /opt/lora-app-server/certs/http.pem -days 365 -nodes")


# Setup as service
def system_daemon_setup():
	# LoRa Gateway Bridge
	run("cp template/lora-gateway-bridge.service /etc/systemd/system/lora-gateway-bridge.service")

	# LoRa Network Server
	run("cp template/loraserver.service /etc/systemd/system/loraserver.service")

	# LoRa App Server
	run("cp template/lora-app-server.service /etc/systemd/system/lora-app-server.service")

# Setup the configuration
def service_config_file():

	# LoRa Gateway Bridge
	run("mkdir /etc/systemd/system/lora-gateway-bridge.service.d")
	run("cp template/lora-gateway-bridge.conf /etc/systemd/system/lora-gateway-bridge.service.d/lora-gateway-bridge.conf")

	# LoRa Network Server
	run("mkdir /etc/systemd/system/loraserver.service.d")
	run("cp template/loraserver.conf /etc/systemd/system/loraserver.service.d/loraserver.conf")

	# LoRa App Server
	run("mkdir /etc/systemd/system/lora-app-server.service.d")
	run("cp template/lora-app-server.conf /etc/systemd/system/lora-app-server.service.d/lora-app-server.conf")

# Restart all services
def restart_all_services():
	# reload daemon
	run("systemctl daemon-reload")

	# Restart LoRa Gateway Bridge, LoRa Network Server and LoRa App Server services
	run("systemctl restart lora-gateway-bridge")
	run("systemctl restart loraserver")
	run("systemctl restart lora-app-server")

# Verify all the services is up and running
def verify_services():
	run("journalctl -u lora-gateway-bridge -f -n 50")
	run("journalctl -u loraserver -f -n 50")
	run("journalctl -u lora-app-server -f -n 50")


# Run it!
def run_all():

	install_package()
	add_system_users()
	create_database()
	download_and_unpack()
	create_certificate()
	system_daemon_setup()
	service_config_file()
	restart_all_services()
	verify_services()







